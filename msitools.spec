%define _version_suffix -f2ae3e2

# The mingw* RPMs are noarch, and the wxi data files are
# arch independant, so it is a waste of CPU cycles to run
# validation on all arches. Just run on x86_64 since that
# has the fastest Fedora builders.
%ifarch x86_64
%define with_validate 1
%else
%define with_validate 0
%endif

Name:           msitools
Version:        0.101.85
Release:        5%{?dist}
Summary:        Windows Installer tools

License:        GPLv2+
URL:            http://ftp.gnome.org/pub/GNOME/sources/%{name}
Source0:        http://ftp.gnome.org/pub/GNOME/sources/%{name}/%{version}/%{name}-%{version}%{?_version_suffix}.tar.gz
# Patch0:         0001-Update-data-wixl.patch

Requires:       libgsf >= 1.14.24-2
Requires:       libmsi1%{?_isa} = %{version}-%{release}

BuildRequires:  git-core
BuildRequires:  meson
BuildRequires:  gettext
BuildRequires:  glib2-devel
BuildRequires:  libgcab1-devel >= 0.2
BuildRequires:  libgsf-devel
BuildRequires:  vala
BuildRequires:  bison

%if %{with_validate}
BuildRequires:  perl-base
BuildRequires:  perl-XML-XPath
BuildRequires:  mingw32-gcc-c++
BuildRequires:  mingw64-gcc-c++
BuildRequires:  mingw32-gcc
BuildRequires:  mingw64-gcc
BuildRequires:  mingw32-gettext
BuildRequires:  mingw64-gettext
BuildRequires:  mingw32-glib2
BuildRequires:  mingw64-glib2
BuildRequires:  mingw32-libffi
BuildRequires:  mingw64-libffi
BuildRequires:  mingw32-pcre
BuildRequires:  mingw64-pcre
BuildRequires:  mingw32-pixman
BuildRequires:  mingw64-pixman
BuildRequires:  mingw32-termcap
BuildRequires:  mingw64-termcap
BuildRequires:  mingw32-win-iconv
BuildRequires:  mingw64-win-iconv
BuildRequires:  mingw32-winpthreads
BuildRequires:  mingw64-winpthreads
BuildRequires:  mingw32-zlib
BuildRequires:  mingw64-zlib
%endif

# https://bugzilla.redhat.com/show_bug.cgi?id=1924216
ExcludeArch: s390x

%description
msitools is a collection of utilities to inspect and create Windows
Installer files.  It is useful in a cross-compilation environment such
as fedora-mingw.

%package -n libmsi1
Summary:        A library to manipulate Windows .MSI files
License:        LGPLv2+

%description -n libmsi1
libmsi is a GObject library to work with Windows Installer files.  It is
a port from the MSI library of the Wine project.

%package -n libmsi1-devel
Summary:        A library to manipulate Windows .MSI files
License:        LGPLv2+
Requires:       libmsi1%{?_isa} = %{version}-%{release}

%description -n libmsi1-devel
The libmsi1-devel package includes the header files for libmsi.

%prep
%autosetup -S git_am -n msitools-%{version}%{?_version_suffix}

%build
%if %{with_validate}
%meson -Dvalidate-wxi=true
%else
%meson
%endif
%meson_build

%install
%meson_install

%find_lang %{name}

%check
%meson_test

%ldconfig_scriptlets -n libmsi1

%files
%doc copyright NEWS README.md TODO
%{_bindir}/msibuild
%{_bindir}/msidiff
%{_bindir}/msidump
%{_bindir}/msiextract
%{_bindir}/msiinfo
%{_bindir}/wixl
%{_bindir}/wixl-heat
%{_datadir}/bash-completion/completions/msitools
%{_datadir}/wixl-%{version}%{?_version_suffix}

%files -n libmsi1 -f %{name}.lang
%{_libdir}/girepository-1.0/Libmsi-1.0.typelib
%{_libdir}/libmsi.so.0*

%files -n libmsi1-devel
%{_datadir}/gir-1.0/Libmsi-1.0.gir
%{_datadir}/vala/vapi/libmsi-1.0.vapi
%{_datadir}/vala/vapi/libmsi-1.0.deps
%{_includedir}/libmsi-1.0/*
%{_libdir}/libmsi.so
%{_libdir}/pkgconfig/libmsi-1.0.pc


%changelog
* Wed Feb 23 2022 Yan Vugenfirer <yvugenfi@redhat.com> - 0.101.85-4
- Add gating and fix gating errors. rhbz#2047292

* Sat Jan 29 2022 Marc-André Lureau <marcandre.lureau@redhat.com> - 0.101.32-3
- Fix FTBFS. rhbz#2046759

* Thu Jan 20 2022 Fedora Release Engineering <releng@fedoraproject.org> - 0.101.32-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_36_Mass_Rebuild

* Sun Aug 01 2021 Marc-André Lureau <marcandre.lureau@redhat.com> - 0.101.32-1
- Fix FTBFS. rhbz#1987721

* Thu Jul 22 2021 Fedora Release Engineering <releng@fedoraproject.org> - 0.101.27-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_35_Mass_Rebuild

* Mon Jun 28 2021 Marc-André Lureau <marcandre.lureau@redhat.com> - 0.101.27-1
- Fix FTBFS. rhbz#1943037

* Wed Feb  3 2021 Daniel P. Berrangé <berrange@redhat.com> - 0.101-2
- Fix enablement of wxi tests
- Use perl-base and gettext for more minimal dep chain

* Tue Feb 02 2021 Marc-André Lureau <marcandre.lureau@redhat.com> - 0.101-1
- new version v0.101
- Fix FTBFS. rhbz#1923683

* Tue Jan 26 2021 Fedora Release Engineering <releng@fedoraproject.org> - 0.100-8
- Rebuilt for https://fedoraproject.org/wiki/Fedora_34_Mass_Rebuild

* Sun Oct 04 2020 Marc-André Lureau <marcandre.lureau@redhat.com> - 0.100-7
- Fix FTBFS. rhbz#1864178
- Drop mingw-gstreamer* checks, it's outdated.

* Sat Aug 01 2020 Fedora Release Engineering <releng@fedoraproject.org> - 0.100-6
- Second attempt - Rebuilt for
  https://fedoraproject.org/wiki/Fedora_33_Mass_Rebuild

* Tue Jul 28 2020 Fedora Release Engineering <releng@fedoraproject.org> - 0.100-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_33_Mass_Rebuild

* Thu May 14 2020 Daniel P. Berrangé <berrange@redhat.com> - 0.100-4
- Switch spice back to dsound instead of wasapi

* Thu Apr 23 2020 Daniel P. Berrangé <berrange@redhat.com> - 0.100-3
- Re-enable wix validation
- Fix bugs in multiple wxi manifests

* Wed Jan 29 2020 Fedora Release Engineering <releng@fedoraproject.org> - 0.100-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_32_Mass_Rebuild

